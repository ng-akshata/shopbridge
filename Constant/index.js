const dotenv = require('dotenv');
dotenv.config();

module.exports = {
    port: process.env.PORT,
    dbName: process.env.DBNAME,
    host: process.env.HOST,
    userName: process.env.USERNAME,
    password: process.env.PASSWORD,
    dbport: process.env.DBPORT,
    upload: './uploads',
    itemImageFolder: './uploads/inventory-item-images',
    getServerUrl: () => {
        return `http://localhost:5555`
    }
};
