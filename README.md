# SHOP BRIDGE #
This repo is for Technical Test.
### Steps to run app.
In order to start app need to do some initial step
###### npm i this will all packages need for this repo.
###### open .env file and set your local env setting for database i.e username and password
###### create databse by name shopdb in postgres.
###### and now last step start server by hitting node index.js. 
#
##### if still got issue check .env file config can be wrong. 

### Package Used.

#### dotenv :
Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env
#### pg :
Non-blocking PostgreSQL client for Node.js. Pure JavaScript and optional native libpq bindings.
#### sequelize:
Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server. It features solid transaction support, relations, eager and lazy loading
