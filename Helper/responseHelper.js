const responseHelper = (res, responseObject) => {
    console.log
    if (responseObject instanceof Error) {
        const { message, statusCode } = responseObject;
        switch (statusCode) {
            case 900:
                res.send({
                    success: false,
                    error: message,
                    statusCode
                });
                break;
            case 901:
                res.send({
                    success: false,
                    error: message,
                    statusCode
                });
                break;
            case 902:
                res.send({
                    success: false,
                    error: message,
                    statusCode
                });
                break;
            default:
        }
    } else {
        res.send({
            success: true,
            data: responseObject.message,
            statusCode: responseObject.statusCode
        });
    }
}

module.exports = {
    responseHelper
}