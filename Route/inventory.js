const { inventory } = require('../Controller');

const baseRouteForInventory = '/api/v1.0/inventory';

const inventoryRoute = (app) => {
    app.post(`${baseRouteForInventory}/create-item`, inventory.addItemToInventory);
    app.get(`${baseRouteForInventory}/get-item-list`, inventory.getItemList);
    app.put(`${baseRouteForInventory}/delete-item/:itemId`, inventory.deleteItem);    
}


module.exports = {
    inventoryRoute
}
