const { inventoryRoute } = require('./inventory');

const setRoute = (app) => {
    inventoryRoute(app);
}

module.exports = {
    setRoute
}