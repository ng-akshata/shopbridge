const multer = require("multer");
const path = require("path");
const { inventory } = require('../Services');
const { responseHelper } = require('../Helper');
const { itemImageFolder } = require('../Constant');
let inventoryError = new Error();

const storage = multer.diskStorage({
    destination(req, file, cb) {
        if (file.fieldname === "image") {
            cb(null, itemImageFolder);
        }
    },
    filename(req, file, cb) {
        if (file.fieldname === "image") {
            cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
        }
    },
});
const upload = multer({
    storage,
}).fields([{
    name: "image",
    limits: { fileSize: 102400 }
}]);


const addItemToInventory = async (req, res) => {
    upload(req, res, async () => {
        const { body, files } = req;
        if (files && files.image) {
            const uploadedImages = req.files.image.map(image => image.filename);
            body.image = uploadedImages.toString();
        }
        const { message, statusCode, success } = await inventory.addItemToInventory(body);

        if (success) {
            responseHelper(res, { message, statusCode })
        } else {
            inventoryError.message = message;
            inventoryError.statusCode = statusCode;
            responseHelper(res, inventoryError)
        }
    });
}

const getItemList = async (req, res) => {
    const { message, statusCode, success } = await inventory.getItemList();
    if (success) {
        responseHelper(res, { message, statusCode })
    } else {
        inventoryError.message = message;
        inventoryError.statusCode = statusCode;
        responseHelper(res, inventoryError)
    }
}

const deleteItem = async (req, res) => {
    const { params: { itemId } } = req;
    const { message, statusCode, success } = await inventory.deleteItem(parseInt(itemId, 10));
    if (success) {
        responseHelper(res, { message, statusCode })
    } else {
        inventoryError.message = message;
        inventoryError.statusCode = statusCode;
        responseHelper(res, inventoryError)
    }
}

module.exports = {
    addItemToInventory,
    getItemList,
    deleteItem,
}