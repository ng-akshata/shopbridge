const Sequelize = require("sequelize");

const { dbName, host, password, userName, dbport } = require('../Constant');

db = {};

const sequelize = new Sequelize(dbName, userName, password, {
    host: host,
    port: dbport,
    dialect: "postgres",
    logQueryParameters: true,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
    logging: console.log,
    define: {
        freezeTableName: true,
    },
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.inventory = require('./inventory')(sequelize, Sequelize);


module.exports = {
    db
}