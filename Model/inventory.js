module.exports = (sequelize, Sequelize) => {
    const inventoryItems = sequelize.define("inventory_items", {
        item_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: Sequelize.STRING,
            defaultValue: '',
            allowNull: false,
        },
        image: {
            type: Sequelize.TEXT,
            defaultValue: '',
            allowNull: true,
        },
        description: {
            type: Sequelize.TEXT,
            defaultValue: '',
            allowNull: true,
        },
        price: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
            allowNull: false,
        },
        is_delete: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        alter: false
    });
    return inventoryItems;
};