const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const { port, upload, itemImageFolder } = require('./Constant');
const { db } = require('./Model');
const { setRoute } = require('./Route');

const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "200mb", extended: true }));

setRoute(app);

const uploadsFolder = [upload, itemImageFolder];
async function createFolder(folder) {
    const localDirectoryFile = folder;
    if (!fs.existsSync(localDirectoryFile)) {
        await fs.mkdirSync(localDirectoryFile);
    }
}
uploadsFolder.map(async (folder) => {
    console.log(folder)
    await createFolder(folder);
});
uploadsFolder.forEach((folderPath) => {
    app.use(express.static(folderPath));
});



db.sequelize.authenticate().then(async () => {
    await db.sequelize.sync();

    app.listen(port, () => {
        console.log(`Server start on ${port}`);
    });
}).catch((error) => {
    throw error;
});
