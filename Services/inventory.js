const { db: { inventory } } = require('../Model');
const { getServerUrl } = require('../Constant');
const Sequelize = require('sequelize');

const addItemToInventory = async (item) => {
    try {
        const itemFound = await inventory.findOne({
            where: Sequelize.where(
                Sequelize.fn('lower', Sequelize.col('name')),
                Sequelize.fn('lower', item.name)
            )
        });
        if (itemFound) {
            return {
                success: false,
                statusCode: 901,
                message: "DATA ALREADY EXIST"
            }
        } else {
            const itemCreated = await inventory.create(item);
            if (itemCreated) {
                return {
                    success: true,
                    statusCode: 200,
                    message: "Item addedd Successfully"
                }
            } else {
                return {
                    success: false,
                    statusCode: 900,
                    message: "Query Failed"
                }
            }
        }
    } catch (err) {
        console.log(err)
        return {
            success: false,
            statusCode: 900,
            message: 'Query Failed'
        }
    }
}

const getItemList = async () => {
    let items = await inventory.findAll({
        where: {
            is_delete: 0
        }
    });
    if (items.length) {
        items.forEach(item => {
            item.image = `${getServerUrl()}/${item.image}`
        });
        return {
            success: true,
            statusCode: 200,
            message: items
        }
    } else {
        return {
            success: false,
            statusCode: 902,
            message: "DATA NOT FOUND"
        }
    }
}

const deleteItem = async (item_id) => {
    const itemFound = await inventory.findOne({
        where: {
            item_id,
            is_delete: 0
        }
    });
    if (itemFound) {
        await inventory.update({ is_delete: 1 }, { where: { item_id } });
        return {
            success: true,
            statusCode: 200,
            message: "Item Deleted."
        }
    } else {
        return {
            success: false,
            statusCode: 902,
            message: "DATA NOT FOUND"
        }
    }
}

module.exports = {
    addItemToInventory,
    getItemList,
    deleteItem
}